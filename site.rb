
require 'sinatra'
require 'erb'

# -------------------------------- Complete ----------
# Payments JS Core
# ---------------

get '/payment/cc' do
  erb :payment
end

get '/payment/ach' do
  erb :coreach
end

get '/payment/vault' do
  erb :vault2
end

get '/payment/vault2' do
  erb :vault3
end

get '/payment/level2' do
  erb :level2
end

get '/payment/rec' do
  erb :rec
end

get '/payment/fsa' do
  erb :fsa
end
# -------------------------------- Complete ----------

# -------------------------------- Complete ----------
# # Payments JS Modal UI
# ---------------

get '/payment/ui/modal/cc' do
  erb :payui
end

get '/payment/ui/modal/check' do
  erb :payach
end

get '/payment/ui/modal/token' do
  erb :vaultui
end
# -------------------------------- Complete ----------

# -------------------------------- Complete ----------

# Payments JS Non-Modal UI
# ---------------

get '/payment/ui/cc' do
  erb :payuinmodal
end

get '/payment/ui/ach' do
   erb :achuinmodel
end

get '/payment/ui/token' do
   erb :vaultuinmodal
end
# -------------------------------- Complete ----------

# -------------------------------- Not Complete ----------
# Payments JS Vault
# ---------------

get '/vault/create' do
 # erb :ToDO
end

get '/payment/read' do
 # erb :ToDO
end

get '/vault/update' do
  # erb :ToDo
end

get '/payment/delete' do
  # erb :ToDO
end
# -------------------------------- Not Complete ----------














